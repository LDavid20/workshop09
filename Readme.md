
# Workshop 09 - Publicar página en un IaaSTarea 

![coamdo tree](img/0.jpg "Archivo de Imagen")  

Para la realizacion de este workshop, inicialmente debemos de tener un cuenta registrada en AWS, este proceso puede llevar entre un 1 dia a una semana, toca ser paciente. Recordar que el correo que vallamos a ustilizar sea un correo estudiantil en este caso el correo que nos proporciona la universidad 


Compredido esto nos dirigimos al siguiente link `https://www.awseducate.com/signin/SiteLogin`, en el cual nos loquearemos luego de confirmada nuestra cuenta

Este seria el inicio que nos mostrara

![coamdo tree](img/1.jpg "Archivo de Imagen")  

Luego nos crearemos un servicio por `EC2`, cabe denotar que el profe destaca la importancia que debe de tener la ubicacion de un servidor. En caso de Costa Rica es mejor el servidores ubicados en Virginia

![coamdo tree](img/2.jpg "Archivo de Imagen")  

Posteriomente realiza distintos test que en distintos sitios que muestran el ping realizados a cada data centeers

![coamdo tree](img/4.jpg "Archivo de Imagen")  

Luego se cambia al servidor del norte de califonia para realizar algunas pruebas

![coamdo tree](img/5.jpg "Archivo de Imagen") 

Ahora procederemos a agregar una nueva instancia

![coamdo tree](img/6.jpg "Archivo de Imagen") 

Para esto escribimos debian en el buscador, cabe denotqar que en la parte inferir podremos obtener mas informacion de la instancia como algun tipo de costo adicional

![coamdo tree](img/7.jpg "Archivo de Imagen") 

LLegado a este punto empezamos  ver los costos

![coamdo tree](img/8.jpg "Archivo de Imagen")
![coamdo tree](img/9.jpg "Archivo de Imagen")

Realizamos a la configuracion de la esta forma

![coamdo tree](img/10.jpg "Archivo de Imagen")

Luego agregaremos algunos tag

![coamdo tree](img/11.jpg "Archivo de Imagen")

Seguidamente la pantalla que sigue vamos a colocar todo de la siguiente forma

![coamdo tree](img/12.jpg "Archivo de Imagen")

Le daremos finalizar a la siguiente pantalla

![coamdo tree](img/13.jpg "Archivo de Imagen")

Esto nos pedira crear o agregar una llave `ssh`

![coamdo tree](img/14.jpg "Archivo de Imagen")

Cabe denotar que si la creamos en el sitio, tenemos que tener en cuenta la ubicacion asignada a la llave

![coamdo tree](img/15.jpg "Archivo de Imagen")

Ya con esto le damos crear, esto llevara algunos minutos en su creacion


![coamdo tree](img/16.jpg "Archivo de Imagen")

Ahora por medio de `namecheap` colocaremos a la ip publica del servicio en el cual estaremos trabajando

![coamdo tree](img/17.jpg "Archivo de Imagen")

Verificamos que se haya creado correctamente el servicio

![coamdo tree](img/18.jpg "Archivo de Imagen")

Copiamos la direccion ip publica que nos brinda

![coamdo tree](img/19.jpg "Archivo de Imagen")

Por medio de la aplicacion colocaremos al ip publica y le asignamos un dominio


![coamdo tree](img/20.jpg "Archivo de Imagen")

Verificamos si esta funcionando

![coamdo tree](img/21.jpg "Archivo de Imagen")

Agreganos la llave que creamos

![coamdo tree](img/22.jpg "Archivo de Imagen")

No dara un fallo, y deberemos de cambiar los permisos. Ahora bien volvemos a ejecutar el comando anterior

![coamdo tree](img/23.jpg "Archivo de Imagen")

Ejecutamos la nueva llave `ssh`

![coamdo tree](img/24.jpg "Archivo de Imagen")

Nos dirigimos al archivo `.bash`

![coamdo tree](img/25.jpg "Archivo de Imagen")

En el modificaremos un alias de esta forma

Luego aplicamos un `apt-get update` para actualizar los paquetes disponibles


![coamdo tree](img/26.jpg "Archivo de Imagen")

Ahora bien instalaremos una serie de librerias, este comando pude tardar unos minutos dependiendo del internet

![coamdo tree](img/27.jpg "Archivo de Imagen")

Luego de esto abrimos una nueva consola y nos dirigimos a la ruta de buster, hay creamos una copia de un archivo `.conf` y el colocaremos el este nombre.

![coamdo tree](img/28.jpg "Archivo de Imagen")

Luego abrimos el documento com `code`, donde le cambiaremos el nombre de la direccion original del archivo por `aws2`

Y asi mismo con el usario que ya nos es vagrant sino admin

![coamdo tree](img/29.jpg "Archivo de Imagen")

Se vuelve a crear otra copia del archivo de la siguiente forma

![coamdo tree](img/30.jpg "Archivo de Imagen")

Luego crear transferimos algun sitio que ya tuvieramos con el siguiente comando, en este caso el sitio esta en un tar.gz


![coamdo tree](img/33.jpg "Archivo de Imagen")

Luego dentro del siguiente arpeta instalamos el composer

![coamdo tree](img/34.jpg "Archivo de Imagen")

Y por ultimo le colocamos el comando final de la guia de instalacion de composer que se encuentra en la paguina oficial

![coamdo tree](img/35.jpg "Archivo de Imagen")

Ahora abrimos el archivo de .bash

![coamdo tree](img/36.jpg "Archivo de Imagen")

Modificamos esta linea de la siguiente forma

![coamdo tree](img/37.jpg "Archivo de Imagen")

Y ahora en la siguiente ruta instalamos el composer

![coamdo tree](img/38.jpg "Archivo de Imagen")

Aplicamos el siguiente comando

![coamdo tree](img/39.jpg "Archivo de Imagen")

Instalamos ala siguiente libreria

![coamdo tree](img/40.jpg "Archivo de Imagen") 

Le aplicamos el comando de install, esto puede tardar un tiempo dependiendo del internet que tengamos

![coamdo tree](img/41.jpg "Archivo de Imagen") 

Ahora si vamos a la ruta que habiamos asignado, ya nos deberia haber cargado el sitio

![coamdo tree](img/42.jpg "Archivo de Imagen") 

Seguimos aplicando mas y mas comando

![coamdo tree](img/43.jpg "Archivo de Imagen") 

Ingresamos el siguiente comando

![coamdo tree](img/44.jpg "Archivo de Imagen") 

Luego habilitamos el siguiente archivo

![coamdo tree](img/45.jpg "Archivo de Imagen") 

Con este comando reiniciamos apache

![coamdo tree](img/46.jpg "Archivo de Imagen") 

Cambiamos los permisos del archivo 

![coamdo tree](img/47.jpg "Archivo de Imagen") 

Entramos al siguiente archivo en la ruta establecida

![coamdo tree](img/48.jpg "Archivo de Imagen") 

Cambiamos la confugiracion de la base de datos

![coamdo tree](img/49.jpg "Archivo de Imagen") 


Luego entramos a la base de datos de `mysql` con el comando de sudo mysql, luego creamos la base de datos con el nombre de laravel

![coamdo tree](img/50.jpg "Archivo de Imagen") 

Creamos un usuario

![coamdo tree](img/51.jpg "Archivo de Imagen") 

Colocamos todos los privilegios

![coamdo tree](img/52.jpg "Archivo de Imagen") 

Refrescamos los privilegios y salimos

![coamdo tree](img/53.jpg "Archivo de Imagen") 

Aplicamos una migracion a la base de datos

![coamdo tree](img/54.jpg "Archivo de Imagen") 

Entramos al archivo de configuracion de la base de datos

![coamdo tree](img/55.jpg "Archivo de Imagen") 

Dentro del archivo comentamos la siguiente linea, guardamos y salimos

![coamdo tree](img/56.jpg "Archivo de Imagen") 

Volvemos a refrescar la base de datos

![coamdo tree](img/57.jpg "Archivo de Imagen") 

Con esto cambios quedaran listo todo














